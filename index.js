document.getElementById("btn-ex1").addEventListener("click", ex1);

function ex1() {
  var income = (document.getElementById("txt-income")?.value * 1) / 1000000;
  var member = document.getElementById("txt-member")?.value * 1;
  var vat = 0;
  var incomeTax = income - 4 - member * 1.6;

  vat =
    incomeTax <= 60
      ? incomeTax * 0.05
      : incomeTax <= 120
      ? incomeTax * 0.1
      : incomeTax <= 210
      ? incomeTax * 0.15
      : incomeTax <= 384
      ? incomeTax * 0.2
      : incomeTax <= 624
      ? incomeTax * 0.25
      : incomeTax <= 960
      ? incomeTax * 0.3
      : incomeTax > 960
      ? incomeTax * 0.35
      : 0;

  vat = vat >= 0 ? Math.round(vat * 1000000) : 0;
  // vat = formatCash(vat);
  console.log({ income, member, incomeTax, vat });
  document.getElementById("result1").innerHTML = `Thuế: ${vat}vnđ`;
}

var typeEl = document.getElementById("select-type");
var connectEl = document.getElementById("txt-num-connect");
connectEl.style.display = "none";
var value = 0;

typeEl.addEventListener("change", () => {
  value = typeEl.value * 1;
  console.log({ value });

  if (value === 2) {
    connectEl.style.display = "block";
  } else {
    connectEl.style.display = "none";
  }
});

function home(numChanel) {
  var feeBill = 4.5;
  var feeNormal = 20.5;
  var feechanel = 7.5 * numChanel;
  var res = feeBill + feeNormal + feechanel;
  return res;
}

function company(numChanel, numConnect) {
  var feeBill = 15;
  var feeNormal = numConnect <= 10 ? 75 : 75 + (numConnect - 10) * 5;
  var feechanel = 50 * numChanel;
  var res = feeBill + feeNormal + feechanel;
  return res;
}

function ex2() {
  var id = document.getElementById("txt-id").value;
  var numChanel = document.getElementById("txt-num").value;
  var numConnect = document.getElementById("txt-num-connect").value;
  var result = null;
  switch (value) {
    case 0:
      result = `0`;
      alert("Bạn chưa chọn loại khách hàng");
      break;
    case 1:
      result = home(numChanel);
      break;
    case 2:
      result = company(numChanel, numConnect);
      break;
  }

  console.log({ result });

  document.getElementById(
    "result2"
  ).innerHTML = `Mã khách hàng: ${id}<br>Tiền cáp: $${result}`;
}

document.getElementById("btn-ex2").addEventListener("click", ex2);
